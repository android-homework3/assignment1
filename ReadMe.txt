Aplicatia prezinta o lista de copii si evolutia acestora pentru o serie de jocuri educative.
1.Technical Contribution
	-pentru securizarea datelor copiilor aplicatia permite o logare sau inregistrare prin Firebase
	-pentru incarcarea listei de copii/jocuri am folosit 2 adaptere 
	-pentru a transmite anumite date din Firebase de la o activitate la alta am folosit intent.putExtra()
	-pentru a usura navigarea intre activitati am creat un meniu in coltul din dreapta care permite delogarea/
intoarcerea la pagina principala
	-pentru a crea cele 2 diagrame de tip BarChart a fost nevoie sa transpun si sa procesez Stringurile din 
Firebase astfel incat sa pot crea date de tip float pentru a fi afisate
	-pentru incarcarea imaginilor am folosit clasa Singleton - VolleyConfigSingleton si un ImageLoader

2.Design Contribution 
	-pentru ca aplicatia sa fie uniforma am folosit o paleta de culori fixa(violet, albatru, turcoaz)
	-am adaugat elemente de tip drawable pentru a avea background-uri si icon-uri unice 
	-am folosit ScrollView-uri pentru ca asezarea in pagina sa fie corecta indiferent de cate de multe date
as avea in lista
	-BarChart-urile au animatie si 2 palete de culori diferite pentru a evidentia cele 2 tipuri de evolutii
	-am folosit margini si am incadrat datele central pentru a umple spatiul

Link-ul catre git pentru clonare:
https://bitbucket.org/android-homework3/assignment1/src/master/
	