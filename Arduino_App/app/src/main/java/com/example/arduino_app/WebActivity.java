package com.example.arduino_app;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.webkit.WebView;

public class WebActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        WebView myWebView = new WebView(this);
        setContentView(myWebView);

        myWebView.loadUrl("https://www.youtube.com/watch?v=Ljuw3grZ11Q");

        setContentView(R.layout.activity_web);
    }
}