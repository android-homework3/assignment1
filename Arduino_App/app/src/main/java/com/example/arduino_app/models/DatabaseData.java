package com.example.arduino_app.models;

public class DatabaseData {

    private String centiseconds;
    private String numberOfFails;
    private String seconds;
    private String currentDate;
    private String currentHour;

    public DatabaseData() {
    }

    public DatabaseData(String centiseconds, String numberOfFails, String seconds, String currentDate, String currentHour) {
        this.centiseconds = centiseconds;
        this.numberOfFails = numberOfFails;
        this.seconds = seconds;
        this.currentDate = currentDate;
        this.currentHour = currentHour;
    }

    public String getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(String currentDate) {
        this.currentDate = currentDate;
    }

    public String getCurrentHour() {
        return currentHour;
    }

    public void setCurrentHour(String currentHour) {
        this.currentHour = currentHour;
    }

    public String getSeconds() {
        return seconds;
    }

    public void setSeconds(String seconds) {
        this.seconds = seconds;
    }

    public String getCentiseconds() {
        return centiseconds;
    }

    public void setCentiseconds(String centiseconds) {
        this.centiseconds = centiseconds;
    }

    public String getNumberOfFails() {
        return numberOfFails;
    }

    public void setNumberOfFails(String numberOfFails) {
        this.numberOfFails = numberOfFails;
    }
}
