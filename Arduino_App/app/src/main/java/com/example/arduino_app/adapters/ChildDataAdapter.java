package com.example.arduino_app.adapters;

import android.telecom.Call;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.example.arduino_app.R;
import com.example.arduino_app.interfaces.OnItemClick;
import com.example.arduino_app.models.ChildData;
import com.example.arduino_app.models.DatabaseData;
import com.example.arduino_app.singleton.VolleyConfigSingleton;

import java.util.ArrayList;

public class ChildDataAdapter extends RecyclerView.Adapter<ChildDataAdapter.LocationsHolder>
{
    private ArrayList<ChildData> children;
    private ItemClickListener itemClickListener;

    private OnItemClick onItemClick;

    public ChildDataAdapter(ArrayList<ChildData> locations, ItemClickListener itemClickListener) {
        this.children = locations;
        this.itemClickListener=itemClickListener;
    }

    @NonNull
    @Override
    public ChildDataAdapter.LocationsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());


        View view = inflater.inflate(R.layout.child_item_cell, parent, false);
        LocationsHolder locationsViewHolder = new LocationsHolder(view);

        return locationsViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ChildDataAdapter.LocationsHolder holder, int position) {

        ChildData locations = (ChildData) this.children.get(position);
        ((ChildDataAdapter.LocationsHolder) holder).bind(locations);

        holder.itemView.setOnClickListener(view->{itemClickListener.onItemClick(position);});
    }

    @Override
    public int getItemCount() {
        return this.children.size();
    }

    public interface ItemClickListener{
        void onItemClick(int position);
    }

    class LocationsHolder extends RecyclerView.ViewHolder {

        private TextView name;
        private TextView age;
        private ImageView imageView;

        View view;

        public LocationsHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.txt_name);
            age = itemView.findViewById(R.id.txt_age);
            imageView = itemView.findViewById(R.id.img_location);
            this.view = itemView;

        }

        void bind(ChildData locationsObj) {

            name.setText(locationsObj.getFullName());
            age.setText(locationsObj.getAge());
            String imageUrl = locationsObj.getUrl().toString();
            ImageLoader imageLoader = VolleyConfigSingleton.getInstance(imageView.getContext().getApplicationContext()).getImageLoader();

            imageLoader.get(imageUrl, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    imageView.setImageBitmap(response.getBitmap());
                }

                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClick != null) {

                        notifyItemChanged(getAdapterPosition());
                    }
                }
            });

        }

    }
}

