package com.example.arduino_app.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.arduino_app.interfaces.OnItemClick;
import com.example.arduino_app.models.DatabaseData;
import com.example.arduino_app.R;


import java.util.ArrayList;

public class DatabaseDataAdapter extends RecyclerView.Adapter<DatabaseDataAdapter.LocationsHolder>
{
    private ArrayList<DatabaseData> locations;

    private OnItemClick onItemClick;

    public DatabaseDataAdapter(ArrayList<DatabaseData> locations) {
        this.locations = locations;
    }

    @NonNull
    @Override
    public DatabaseDataAdapter.LocationsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());


        View view = inflater.inflate(R.layout.location_item_cell, parent, false);
        LocationsHolder locationsViewHolder = new LocationsHolder(view);

        return locationsViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull DatabaseDataAdapter.LocationsHolder holder, int position) {

        DatabaseData locations = (DatabaseData) this.locations.get(position);
        ((DatabaseDataAdapter.LocationsHolder) holder).bind(locations);
    }

    @Override
    public int getItemCount() {
        return this.locations.size();
    }

    class LocationsHolder extends RecyclerView.ViewHolder {

        private TextView centiseconds;
        private TextView numberOfFails;
        private TextView seconds;
        private TextView currentDate;
        private TextView currentHour;

        View view;

        public LocationsHolder(@NonNull View itemView) {
            super(itemView);

            centiseconds = itemView.findViewById(R.id.txt_centiseconds);
            numberOfFails = itemView.findViewById(R.id.txt_numerOfFails);
            seconds = itemView.findViewById(R.id.txt_seconds);
            currentDate=itemView.findViewById(R.id.txt_date);
            currentHour=itemView.findViewById(R.id.txt_hour);
            this.view = itemView;

        }

        void bind(DatabaseData locationsObj) {

            centiseconds.setText(locationsObj.getCentiseconds());
            numberOfFails.setText(locationsObj.getNumberOfFails());
            seconds.setText(locationsObj.getSeconds());
            currentDate.setText(locationsObj.getCurrentDate());
            currentHour.setText(locationsObj.getCurrentHour());

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemClick != null) {

                        notifyItemChanged(getAdapterPosition());
                    }
                }
            });

        }

    }
}

