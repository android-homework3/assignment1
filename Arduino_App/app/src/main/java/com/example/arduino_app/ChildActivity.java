package com.example.arduino_app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.example.arduino_app.adapters.DatabaseDataAdapter;
import com.example.arduino_app.fragments.RecyclerViewFragment;
import com.example.arduino_app.models.DatabaseData;
import com.example.arduino_app.singleton.VolleyConfigSingleton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class ChildActivity extends AppCompatActivity {

    FirebaseDatabase database = FirebaseDatabase.getInstance("https://arduino-a0097-default-rtdb.firebaseio.com/");
    DatabaseReference myRef = database.getReference().child("Children");
    ArrayList<DatabaseData> databaseDataArrayList = new ArrayList<DatabaseData>();

    RecyclerView recyclerView;

    DatabaseDataAdapter databaseDataAdapter;
    String id;
    String fullName;
    String age;
    String url;
    String description;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child);

        TextView textView=findViewById(R.id.text_child);
        TextView textAge=findViewById(R.id.text_age);
        imageView=findViewById(R.id.img_location);

        id= "id";
        fullName= "id";
        age= "id";
        url="id";
        description="id";
        Bundle extras=getIntent().getExtras();
        if(extras!=null)
        {
            id=extras.getString("id");
            fullName=extras.getString("fullName");
            age=extras.getString("age");
            url=extras.getString("url");
            description=extras.getString("description");
        }

        ImageLoader imageLoader = VolleyConfigSingleton.getInstance(imageView.getContext().getApplicationContext()).getImageLoader();

        imageLoader.get(url, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                imageView.setImageBitmap(response.getBitmap());
            }

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        textView.setText(fullName+", ");
        textAge.setText(age+" years old");

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView = (RecyclerView) findViewById(R.id.database_data_list);
        recyclerView.setLayoutManager(linearLayoutManager);
        getDataFromDatabase(id);

        databaseDataAdapter = new DatabaseDataAdapter(databaseDataArrayList);
        recyclerView.setAdapter(databaseDataAdapter);

        findViewById(R.id.btn_charts).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                goToChartActivity();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.right_menu_evolution, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()) {
            case R.id.log_out:
                FirebaseAuth.getInstance().signOut();
                goToWelcome();
                return true;
            case R.id.page_home:
                goToRecyclerViewFragment();
                return true;
            default: return  super.onOptionsItemSelected(item);
        }
    }

    private void goToWelcome() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void goToRecyclerViewFragment() {
        Fragment fragment = new RecyclerViewFragment();
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.child_frame_layout, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void goToChartActivity() {
        Intent intent = new Intent(this, ChartActivity.class);
        intent.putExtra("id",id);
        intent.putExtra("fullName",fullName);
        intent.putExtra("age",age);
        intent.putExtra("description",description);
        startActivity(intent);

        this.finish();
    }

    public void getDataFromDatabase(String child) {
        databaseDataArrayList.clear();
        DatabaseReference reference = myRef.child(child).child("plays");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                databaseDataArrayList.clear();
                for (DataSnapshot ds : snapshot.getChildren()) {
                    DatabaseData database = ds.getValue(DatabaseData.class);
                    databaseDataArrayList.add(database);
                }
                databaseDataAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }
}