package com.example.arduino_app.models;

import java.util.ArrayList;
import java.util.List;

public class ChildData {
    String fullName;
    String age;
    String id;
    String url;
    String description;
    ArrayList<DatabaseData> children=new ArrayList<>();

    public ChildData() {
    }

    public ChildData(String fullName, String age) {
        this.fullName = fullName;
        this.age = age;
    }

    public ChildData(String fullName, String age, ArrayList<DatabaseData> children) {
        this.fullName = fullName;
        this.age = age;
        this.children = children;
    }

    public ChildData(String fullName, String age, String id, ArrayList<DatabaseData> children) {
        this.fullName = fullName;
        this.age = age;
        this.id = id;
        this.children = children;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public ArrayList<DatabaseData> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<DatabaseData> children) {
        this.children = children;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
