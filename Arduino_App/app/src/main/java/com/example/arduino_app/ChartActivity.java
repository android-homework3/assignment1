package com.example.arduino_app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.arduino_app.fragments.RecyclerViewFragment;
import com.example.arduino_app.models.DatabaseData;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ChartActivity extends AppCompatActivity {

    String id;
    String fullName;
    String age;
    String description;
    BarChart barChart;
    BarChart barChartFails;
    FirebaseDatabase database = FirebaseDatabase.getInstance("https://arduino-a0097-default-rtdb.firebaseio.com/");
    DatabaseReference myRef = database.getReference().child("Children");
    ArrayList<DatabaseData> databaseDataArrayList = new ArrayList<>();

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.right_menu_evolution, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()) {
            case R.id.log_out:
                FirebaseAuth.getInstance().signOut();
                goToWelcome();
                return true;
            case R.id.page_home:
                goToRecyclerViewFragment();
                return true;
            default: return  super.onOptionsItemSelected(item);
        }
    }

    private void goToRecyclerViewFragment() {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    private void goToWelcome() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart);

        TextView textView=findViewById(R.id.text_child);
        TextView textAge=findViewById(R.id.text_age);
        TextView textDescription=findViewById(R.id.txt_description);

        id= "id";
        fullName= "id";
        age= "id";
        description="id";
        Bundle extras=getIntent().getExtras();
        if(extras!=null)
        {
            id=extras.getString("id");
            fullName=extras.getString("fullName");
            age=extras.getString("age");
            description=extras.getString("description");
        }
        textView.setText(fullName+", ");
        textAge.setText(age+" years old");
        textDescription.setText(description);

        barChart = findViewById(R.id.barChart);
        barChartFails = findViewById(R.id.barChartFails);
        getTimeforChart(id);

        findViewById(R.id.btn_web).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                goToWebActivity();
            }
        });
    }

    private void goToWebActivity() {
        Intent intent = new Intent(this, WebActivity.class);
        startActivity(intent);

        this.finish();
    }

    public void getTimeforChart(String child) {
        databaseDataArrayList.clear();
        DatabaseReference reference = myRef.child(child).child("plays");
        ArrayList<BarEntry> barEntries = new ArrayList<>();
        ArrayList<BarEntry> barEntriesFails = new ArrayList<>();
        final ArrayList<String> xAxisLabel = new ArrayList<>();
        xAxisLabel.add("0");

        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                databaseDataArrayList.clear();
                int i = 1;
                for (DataSnapshot ds : snapshot.getChildren()) {
                    DatabaseData database = ds.getValue(DatabaseData.class);
                    float centiseconds = (float) Float.parseFloat(database.getCentiseconds());
                    int seconds = (int) Integer.parseInt(database.getSeconds());
                    float finalCenti = centiseconds / 100;
                    float result = seconds + finalCenti;
                    String s = "" + result;
                    float value = (float) Float.parseFloat(s);
                    BarEntry barEntry = new BarEntry(i, value);
                    barEntries.add(barEntry);

                    float numberOfFails = (float) Float.parseFloat(database.getNumberOfFails());
                    BarEntry barEntryFail = new BarEntry(i, numberOfFails);
                    barEntriesFails.add(barEntryFail);

                    String string = database.getCurrentDate();
                    String[] array = string.split("[.]", 0);
                    String date = array[0] + "." + array[1];
                    String string1 = database.getCurrentHour();
                    String[] array1 = string1.split("[:]", 0);
                    String hour = array1[0] + ":" + array1[1];
                    String finalString = date + ", " + hour;

                    xAxisLabel.add(finalString);
                    i++;
                }

                BarDataSet barDataSet = new BarDataSet(barEntries, "Time / game");
                barDataSet.setColors(ColorTemplate.COLORFUL_COLORS);
                BarData barData = new BarData(barDataSet);
                barChart.setData(barData);
                barChart.animateY(5000);
                barChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(xAxisLabel));
                barChart.getXAxis().setLabelRotationAngle(270);
                barChart.setExtraTopOffset(45);
                barChart.getXAxis().setTextColor(Color.MAGENTA);
                barChart.getXAxis().setTextSize(10);
                barChart.getAxisRight().setEnabled(false);
                barChart.getDescription().setEnabled(false);

                BarDataSet barDataSetFails = new BarDataSet(barEntriesFails, "Number of fails / game");
                barDataSetFails.setColors(ColorTemplate.PASTEL_COLORS);
                barChartFails.setData(new BarData(barDataSetFails));
                barChartFails.animateY(5000);
                barChartFails.getXAxis().setLabelRotationAngle(270);
                barChartFails.setExtraTopOffset(45);
                barChartFails.getXAxis().setValueFormatter(new IndexAxisValueFormatter(xAxisLabel));
                barChartFails.getXAxis().setTextColor(Color.MAGENTA);
                barChartFails.getXAxis().setTextSize(10);
                barChartFails.getAxisRight().setEnabled(false);
                barChartFails.getDescription().setEnabled(false);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }
}