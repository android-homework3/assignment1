package com.example.arduino_app.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.arduino_app.ChartActivity;
import com.example.arduino_app.ChildActivity;
import com.example.arduino_app.R;
import com.example.arduino_app.adapters.ChildDataAdapter;
import com.example.arduino_app.interfaces.ActivitiesFragmentsCommunication;
import com.example.arduino_app.models.ChildData;
import com.example.arduino_app.models.DatabaseData;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RecyclerViewFragment extends Fragment {

    public static final String TAG_RECYCLERVIEW_FRAGMENT = "TAG_RECYCLERVIEW_FRAGMENT";

    FirebaseDatabase database = FirebaseDatabase.getInstance("https://arduino-a0097-default-rtdb.firebaseio.com/");
    DatabaseReference myRef = database.getReference().child("Children");
    ArrayList<ChildData> childrenList =new ArrayList<>();
    RecyclerView recyclerView;

    ChildDataAdapter childDataAdapter;
    public RecyclerViewFragment() {
    }

    public static RecyclerViewFragment newInstance() {

        Bundle args = new Bundle();
        RecyclerViewFragment fragment = new RecyclerViewFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recycler_view, container, false);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView = (RecyclerView) view.findViewById(R.id.database_data_list);
        recyclerView.setLayoutManager(linearLayoutManager);

        getDataFromDatabase();
        childDataAdapter = new ChildDataAdapter(childrenList, new ChildDataAdapter.ItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(getActivity(), ChildActivity.class);
                intent.putExtra("id",childrenList.get(position).getId());
                intent.putExtra("fullName",childrenList.get(position).getFullName());
                intent.putExtra("age",childrenList.get(position).getAge());
                intent.putExtra("url",childrenList.get(position).getUrl());
                intent.putExtra("description",childrenList.get(position).getDescription());

                startActivity(intent);
                getActivity().finish();
            }
        });
        recyclerView.setAdapter(childDataAdapter);

        view.findViewById(R.id.btn_charts).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                goToChartActivity();
            }
        });
        return view;
    }

    private void getDataFromDatabase() {

        childrenList.clear();
        DatabaseReference reference = myRef;
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                childrenList.clear();
                for (DataSnapshot ds : snapshot.getChildren()) {
                    ChildData database = ds.getValue(ChildData.class);
                    childrenList.add(database);
                }
                childDataAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }

    private void goToChartActivity() {
        Intent intent = new Intent(getActivity(), ChartActivity.class);
        startActivity(intent);

        getActivity().finish();
    }
}